import React from 'react';
import {View, Text, StyleSheet, Image, ScrollView} from 'react-native';
import GetlookStyles from './GetlookStyles';
import SearchBar from 'react-native-search-bar';
import face_pack from './assests/app1.png';
import image1 from './assests/image1.png';
import image2 from './assests/image2.png';
import image3 from './assests/image3.png';
import app_paypal1 from './assests/app_paypal.png';
import beauty_services1 from './assests/choose_beauty_services1.png';
import beauty_services2 from './assests/choose_beauty_services2.png';
import getlook_logo from './assests/Getlook_logo.png';
import left_arrow from './assests/left_arrow.png';
import right_arrow from './assests/right_arrow.png';
import menu1 from './assests/menu1.png';
import menu_vertical from './assests/menu_vertical1.png';
import bag_icon from './assests/bag.png';

const styles = StyleSheet.create(GetlookStyles);
const Screen2 = () => {
  return (
    <View style={styles.mainContainer}>
      <View style={styles.header_footer_style}>
        <View style={styles.header_logo}>
          <Image style={styles.menu} source={menu1} />
          <Image style={styles.logo} source={getlook_logo} />
          <Image style={styles.bag_icon} source={bag_icon} />
          <Image style={styles.menu_vertical} source={menu_vertical} />
        </View>
      </View>
      <View style={styles.search_bar}>
        <SearchBar
          placeholder="Search"
          //   onChangeText={...}
          //   onSearchButtonPress={...}
          //   onCancelButtonPress={...}
        />
      </View>
      <ScrollView>
        <View>
          <Image style={styles.face_pack} source={face_pack} />
        </View>
        <View style={styles.Shop_by_category}>
          <Text style={styles.text}>Shop By Category</Text>
        </View>
        <View style={styles.images}>
          <Image style={styles.left_arrow} source={left_arrow} />
          <Image style={styles.Category_images1} source={image1} />
          <Image style={styles.Category_images1} source={image2} />
          <Image style={styles.Category_images3} source={image3} />
          <Image style={styles.right_arrow} source={right_arrow} />
        </View>
        <View>
          <Image style={styles.offer_cashback} source={app_paypal1} />
        </View>
        <View style={styles.packages}>
          <Text style={styles.package_text}>Packages</Text>
          <Text style={styles.package_text1}>View all</Text>
        </View>
        <View>
          <Image style={styles.choose_btservices1} source={beauty_services1} />
          <Image style={styles.choose_btservices2} source={beauty_services2} />
        </View>
      </ScrollView>
    </View>
  );
};

export default Screen2;
