/**
 * @format
 */

import {AppRegistry} from 'react-native';
// import Screen1 from './Screen1';
import {name as appName} from './app.json';
import Screen2 from './Screen2';

AppRegistry.registerComponent(appName, () => Screen2);
