const GetlookStyles = {
  mainContainer: {
    backgroundColor: '#EFF0F2',
  },
  header_footer_style: {
    width: '100%',
    height: 50,
    backgroundColor: 'white',
  },
  header_logo: {
    flexDirection: 'row',
  },
  logo: {
    marginLeft: 20,
  },
  menu: {
    marginLeft: 10,
    // marginTop: 5,
  },
  bag_icon: {
    marginLeft: 110,
    // marginTop: 10,
  },
  menu_vertical: {
    marginTop: 2,
  },
  Shop_by_category: {
    width: '95%',
    height: 30,
    backgroundColor: 'white',
    borderWidth: 0.5,
    marginLeft: 10,
    marginTop: 10,
    // marginLeft:7,
    // marginTop:"-18%"
  },
  text: {
    textAlign: 'center',
    marginTop: 5,
  },
  images: {
    flexDirection: 'row',
  },
  packages: {
    width: '95%',
    height: 30,
    backgroundColor: 'white',
    borderWidth: 0.5,
    marginLeft: 10,
  },
  package_text: {
    textAlign: 'center',
    marginTop: 5,
  },
  package_text1: {
    marginLeft: '85%',
    marginTop: -20,
  },
  beauty_services: {
    flexDirection: 'row',
  },
  face_pack: {
    // height:'70%',
    //  width:'100%',
    marginTop: 10,
  },
  Category_images1: {
    height: '100%',
    width: '30%',
    marginTop: 10,
  },
  Category_images3: {
    height: 100,
    width: '30%',
    marginTop: 10,
  },
  offer_cashback: {
    height: 50,
    width: 400,
    marginTop: 10,
    marginLeft: 5,
  },
  left_arrow: {
    marginTop: 50,
  },
  right_arrow: {
    marginTop: 50,
  },
  choose_btservices1: {
    // height:300,
    // width:400,
    marginTop: 10,
    marginLeft: 45,
  },
  choose_btservices2: {
    // height:300,
    // width:400,
    // marginTop:10,
    // marginLeft:10
    marginTop: 10,
    marginLeft: 45,
  },
  search_bar: {
    // borderWidth: 1,
    marginTop: 10,
    borderRadius: 50,
    width: 400,
    marginLeft: 5,
  },
  login: {
    flexDirection: 'row',
    paddingVertical: 30,
    paddingHorizontal: 20,
  },
  signin: {
    backgroundColor: '#000000',
    marginTop: 350,
    height: 300,
    opacity: 0.6,
  },
  wallpaper_container: {
    padding: -10,
    flex: 5,
    width: 2,
  },
  wallpaper: {
    width: 450,
    height: 600,
  },
  login_text: {
    color: 'white',
    textAlign: 'center',
    fontSize: 20,
    marginTop: 5,
  },
  facebookButton: {
    width: 150,
  },
  googleButton: {
    paddingHorizontal: 30,
    width: 220,
  },
  mobilenumber_input: {
    backgroundColor: 'white',
    width: 350,
    marginLeft: 20,
    textAlign: 'center',
    borderColor: '#707070',
    borderWidth: 1,
    color: '#A5A5A5',
  },
  facebook_text: {
    backgroundColor: 'white',
    borderRadius: 50,
    width: 30,
    height: 30,
    textAlign: 'center',
    fontSize: 25,
    color: 'blue',
  },
};
export default GetlookStyles;
