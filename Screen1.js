/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, Image, Button, TextInput, StyleSheet} from 'react-native';
import getlook from './assests/getlook.png';
import customer_wallpaper from './assests/wallpaper.png';
import GetlookStyles from './GetlookStyles';
// import {Link} from 'react-router';

const styles = StyleSheet.create(GetlookStyles);
const Screen1 = navigation => {
  return (
    <View>
      <View style={styles.wallpaper_container}>
        <Image style={styles.wallpaper} source={customer_wallpaper} />
      </View>
      <View>
        <Image source={getlook} />
      </View>
      <View style={styles.signin}>
        <Text style={styles.login_text}>LOGIN/SIGNUP USING</Text>
        <View style={styles.login}>
          <View style={styles.facebookButton}>
            {/* <Text style={styles.facebook_text}>f</Text> */}
            <Button color="#3B5998" title="facebook" />
          </View>
          <View style={styles.googleButton}>
            <Button color="#ff5c5c" title="google" />
          </View>
        </View>
        <TextInput
          style={styles.mobilenumber_input}
          placeholder="Enter Mobile No"
        />
        {/* <Link to="/Screen2">React</Link> */}
      </View>
    </View>
  );
};

export default Screen1;
